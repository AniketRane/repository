package Runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "C:\\zenqgit\\mystorage\\src\\test\\java\\features"
        ,glue={"StepDefinatiion"},
        format = {"pretty","html:test-output"}

)
public class Testfile {
}
