package Sample;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SampleTestPage
{
    WebDriver driver;
    @BeforeTest
    public void testprogram()
    {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
        driver = new ChromeDriver();

        driver.get("https://www.testandquiz.com/selenium/testing.html");

        driver.manage().window().maximize();
    }
    @Test(priority = 1)
    void link()
    {
        driver.findElement(By.cssSelector("[href$='javatpoint.com/']")).click();
        driver.navigate().back();
    }

    @Test(priority = 2)
    void textbox()
    {
        WebElement element= driver.findElement(By.cssSelector("[id='fname']"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');",element);
        element.sendKeys("Hello There");
    }
    @Test(priority = 3)
    void submit()
    {
        driver.findElement(By.cssSelector("[title='Click me!!']")).click();
        driver.navigate().refresh();
    }
    @Test(priority = 4)
    void radiobutton()
    {
        driver.findElement(By.cssSelector("[id='female']")).click();
        driver.findElement(By.cssSelector("[id='male']")).click();
    }
    @Test(priority = 5)
    void checkbox()
    {
        driver.findElement(By.cssSelector("[class='Automation']")).click();
        driver.findElement(By.cssSelector("[class='Performance']")).click();
    }
    @Test(priority = 6)
    void dropdown()
    {
        WebElement element=driver.findElement(By.cssSelector("[id='testingDropdown']"));

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');",element);

        Select dropdown=new Select(element);
       dropdown.selectByVisibleText("Manual Testing");

        js.executeScript("scrollBy(0, 4500)");
    }
    @Test(priority = 7)
    void handlingalerts()
    {
        driver.findElement(By.xpath("//button[text()='Generate Alert Box']")).click();
        Alert alert=driver.switchTo().alert();
        alert.accept();

        driver.findElement((By.xpath("//button[text()='Generate Confirm Box']"))).click();
        Alert confirmbox=driver.switchTo().alert();
        confirmbox.dismiss();
    }
    @Test(priority = 8)
    void draganddrop()
    {
        WebElement from=driver.findElement(By.cssSelector("[id='sourceImage']"));
        WebElement to=driver.findElement((By.cssSelector("[id='targetDiv']")));

        Actions action=new Actions(driver);
        action.clickAndHold(from);
        action.moveToElement(to);
        action.release();
        action.build();
       // action.dragAndDrop(from,to).build().perform();
    }

}
