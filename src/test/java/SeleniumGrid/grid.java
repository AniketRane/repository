package SeleniumGrid;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.*;
public class grid {
    WebDriver driver;
    String baseURL, nodeURL;
    @BeforeTest
    public void setUp() throws MalformedURLException {
        baseURL = "https://demo.opencart.com/";
        nodeURL = "http://192.168.43.87:2222/wd/hub";
        DesiredCapabilities capability = DesiredCapabilities.chrome();
        capability.setBrowserName("chrome");
        capability.setPlatform(Platform.WINDOWS);
        driver = new RemoteWebDriver(new URL(nodeURL), capability);
    }
    @Test
    public void sampleTest() {
        driver.get(baseURL);
    }

}
