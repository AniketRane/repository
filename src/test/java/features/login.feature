Feature: Your Store Login page

  Scenario Outline : Your Store Login Test Scenario
    Given user is already on login page
    When title of login page is Account Login
    Then user enters "<username>" and "<password>"
    Then user click on login button
    Then user is on My Account page
    Then close the browser

    Examples:
      | username                | password   |
      | aniketrane003@gmail.com | HelloWorld |

