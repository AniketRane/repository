package Basics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class LoginLogicClass
{
   WebDriver driver;
   By getstarted=By.cssSelector("[class='list-unstyled'] [href$='account']");
   By getstarted1= By.cssSelector("[name='email']");
   By getstarted3=By.cssSelector("[name='password']");
   By getstarted4=By.cssSelector("[value='Login']");

   public LoginLogicClass(WebDriver driver)
    {
    this.driver=driver;
    }
 public void login()
    {
        Actions action =new Actions(driver);
        action.moveToElement(driver.findElement(getstarted)).build().perform();
        driver.findElement(getstarted).click();
    }
    public void email(String arg1)
    {
        Actions action =new Actions(driver);
        action.moveToElement(driver.findElement(getstarted1)).build().perform();
     driver.findElement(getstarted1).sendKeys(arg1);
    }
    public void password(String arg2)
    {
        Actions action =new Actions(driver);
        action.moveToElement(driver.findElement(getstarted3)).build().perform();
        driver.findElement(getstarted3).sendKeys(arg2);
    }
    public void mainlogin()
    {
        Actions action =new Actions(driver);
        action.moveToElement(driver.findElement(getstarted4)).build().perform();
        driver.findElement(getstarted4).click();
    }


}
