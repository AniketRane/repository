package Basics;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;


import java.util.Iterator;
import java.util.Set;

public class NavigationLogicClass
{
    WebDriver driver;
    By getme= By.cssSelector("[id='logo'] [href$='home']");
    By getme1=By.cssSelector("[href$='category&path=25'][class='dropdown-toggle']");
    By getme2=By.cssSelector("[href$='product/category&path=25_28']");
    public NavigationLogicClass(WebDriver driver)
    {
        this.driver=driver;
    }
     public void  navigatehome()
     {
         Actions action=new Actions(driver);
         action.moveToElement(driver.findElement(getme)).build().perform();
         driver.findElement(getme).click();
     }
     public void components()
     {
         Actions action=new Actions(driver);
         action.moveToElement(driver.findElement(getme1)).build().perform();
         driver.findElement(getme1).click();
         action.moveToElement(driver.findElement(getme2)).build().perform();

         String click = Keys.chord(Keys.CONTROL, Keys.ENTER);
         driver.findElement(getme2).sendKeys(click);

     }
     public void switchwindow(){
         String parent = driver.getWindowHandle();

         Set<String> s = driver.getWindowHandles();
         Iterator<String> I1 = s.iterator();

         while (I1.hasNext())
         {

             String child_window = I1.next();


             if (!parent.equals(child_window)) {
                 driver.switchTo().window(child_window);
             }
         }
         //driver.switchTo().window(parent);


     }
}
