package Basics;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class AddcartLogicClass {
    WebDriver driver;
    By get=By.cssSelector("[name='search']");
    By get1=By.cssSelector("button[class='btn btn-default btn-lg']");
    By get2=By.cssSelector("[class='caption'] [href*='id=43']");
    By get3=By.cssSelector("[id='button-cart']");
    By get4=By.cssSelector("[id='cart'] [data-toggle='dropdown']");


    public AddcartLogicClass(WebDriver driver)
    {
        this.driver=driver;
    }
void addcart(){
        Actions action =new Actions(driver);

        action.moveToElement(driver.findElement(get)).build().perform();
        driver.findElement(get).sendKeys("MacBook");

        action.moveToElement(driver.findElement(get1)).build().perform();
        driver.findElement(get1).sendKeys(Keys.ENTER);

        action.moveToElement(driver.findElement(get2)).build().perform();
        driver.findElement(get2).click();

        action.moveToElement(driver.findElement(get3)).build().perform();
        driver.findElement(get3).click();

        action.moveToElement(driver.findElement(get4)).build().perform();
        driver.findElement(get4).click();
    }
}