package Basics;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class OpencartAutomation
{
    WebDriver driver;
    LoginLogicClass l;
    AddcartLogicClass a;
    NavigationLogicClass h;

    @BeforeTest
    public void testprogram()
    {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
        driver = new ChromeDriver();

        driver.get("https://demo.opencart.com/");

      driver.manage().window().maximize();
      }

      @Test(priority = 1)
    public void LoginIntoAccount()
    {

     l=new LoginLogicClass(driver);
       l.login();
     l.email("aniketrane003@gmail.com");
     l.password("HelloWorld");
     l.mainlogin();
    }
    @Test(priority = 2)
    public void addtocart()
    {
        a=new AddcartLogicClass(driver);
        a.addcart();
    }
    @Test(priority = 3)
    public void homepage(){
        h=new NavigationLogicClass(driver);
        h.navigatehome();
        h.components();
        h.switchwindow();
    }


}
