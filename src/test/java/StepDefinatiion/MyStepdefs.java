package StepDefinatiion;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class MyStepdefs {
    WebDriver driver;
    @Given("user is already on login page")
    public void userIsAlreadyOnLoginPage()
    {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
        driver=new ChromeDriver();
        driver.navigate().to("https://demo.opencart.com/index.php?route=account/login");
    }

    @When("title of login page is Account Login")
    public void titleOfLoginPageIsAccountLogin()
    {
        String loginTitle=    driver.getTitle();
        System.out.println(loginTitle);
        Assert.assertEquals("Account Login",loginTitle);
    }

    @Then("user enters {string} and {string}")
    public void userEntersAnd(String user, String pass)
    {
        driver.findElement(By.id("input-email")).sendKeys(user);
        driver.findElement(By.id("input-password")).sendKeys(pass);
    }

    @Then("user click on login button")
    public void userClickOnLoginButton()
    {
        driver.findElement(By.cssSelector("form>input.btn")).click();
    }

    @Then("user is on My Account page")
    public void userIsOnMyAccountPage() {
        String  title= driver.getTitle();
        Assert.assertEquals("My Account",title);
        System.out.println("title is matched");
    }

    @Then("close the browser")
    public void closeTheBrowser() {


    }
}
