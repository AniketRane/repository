package Browser;

import org.testng.annotations.Test;

public class TestNGBrowsers {
    @Test
    public void testProgram()
    {
        LogicClassBrowser chrome=new LogicClassBrowser();
        chrome.chrome();
    }
    @Test
    public void testProgram1()
    {
        LogicClassBrowser firefox=new LogicClassBrowser();
        firefox.firefox();
    }
    @Test
    public void testProgram2()
    {
        LogicClassBrowser edge=new LogicClassBrowser();
        edge.microsoftedge();
    }
}

