package Browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class LogicClassBrowser {
    void chrome()
    {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://gitlab.com/");
        String title = driver.getTitle();//to get title of website
        System.out.println(title);

        String curenturl = driver.getCurrentUrl();
        System.out.println(curenturl);

        String getpagesource = driver.getPageSource();
        System.out.println(getpagesource);

        // driver.close();
        //driver.quit();
    }
    void firefox()
    {
        System.setProperty("webdriver.gecko.driver","drivers/geckodriver.exe");
        WebDriver driver2=new FirefoxDriver();
        driver2.get("https://gitlab.com/");
        String title2=driver2.getTitle();//to get title of website
        System.out.println(title2);

        String curenturl2=driver2.getCurrentUrl();
        System.out.println(curenturl2);

        String getpagesource2=driver2.getPageSource();
        System.out.println(getpagesource2);

//         driver2.close();
//          driver2.quit();
    }
    void microsoftedge()
    {
        System.setProperty("webdriver.edge.driver", "drivers/msedgedriver.exe");
        WebDriver driver1=new EdgeDriver();
        driver1.get("https://gitlab.com/");
        String title1=driver1.getTitle();//to get title of website
        System.out.println(title1);

        String curenturl1=driver1.getCurrentUrl();
        System.out.println( curenturl1);

        String getpagesource1=driver1.getPageSource();
        System.out.println(getpagesource1);

        // driver1.close();
        // driver1.quit();
    }
}
