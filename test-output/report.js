$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("login.feature");
formatter.feature({
  "line": 1,
  "name": "Your Store Login page",
  "description": "",
  "id": "your-store-login-page",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 2,
  "name": "Your Store Login Test Scenario",
  "description": "",
  "id": "your-store-login-page;your-store-login-test-scenario",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 3,
  "name": "user is already on login page",
  "keyword": "Given "
});
formatter.step({
  "line": 4,
  "name": "title of login page is Account Login",
  "keyword": "When "
});
formatter.step({
  "line": 5,
  "name": "user enters \"\u003cusername\u003e\" and \"\u003cpassword\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "user click on login button",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "user is on My Account page",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "close the browser",
  "keyword": "Then "
});
formatter.examples({
  "line": 10,
  "name": "",
  "description": "",
  "id": "your-store-login-page;your-store-login-test-scenario;",
  "rows": [
    {
      "cells": [
        "username",
        "password"
      ],
      "line": 11,
      "id": "your-store-login-page;your-store-login-test-scenario;;1"
    },
    {
      "cells": [
        "aniketrane003@gmail.com",
        "HelloWorld"
      ],
      "line": 12,
      "id": "your-store-login-page;your-store-login-test-scenario;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 12,
  "name": "Your Store Login Test Scenario",
  "description": "",
  "id": "your-store-login-page;your-store-login-test-scenario;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 3,
  "name": "user is already on login page",
  "keyword": "Given "
});
formatter.step({
  "line": 4,
  "name": "title of login page is Account Login",
  "keyword": "When "
});
formatter.step({
  "line": 5,
  "name": "user enters \"aniketrane003@gmail.com\" and \"HelloWorld\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "user click on login button",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "user is on My Account page",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "close the browser",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
});